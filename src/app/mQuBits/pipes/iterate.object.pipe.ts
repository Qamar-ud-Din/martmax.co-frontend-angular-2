/**
 * @author Mustafa Omran m.omran@mqubits.com
 */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'IterateObject',  pure: false })
export class IterateObject implements PipeTransform {
    transform(value: any, args?: any[]): any[] {
      
      if(value) {
        let keyArr: any[] = Object.keys(value),
            dataArr = [];
        keyArr.forEach((key: any) => {
            dataArr.push(value[key]);
        });
        return dataArr;
      }
    }
}