import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

// Load the implementations that should be tested
import { CouponComponent } from './coupon.component';

describe('Coupon', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      CouponComponent
    ]
  }));

  it('should log ngOnInit', inject([CouponComponent], (coupon: CouponComponent) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();
    coupon.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
