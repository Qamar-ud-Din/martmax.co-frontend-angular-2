/**
 * @author Mustafa Qamar-ud-Din <m.qamaruddin@mQuBits.com>
 *  @author Mustafa Omran <m.omran@mQuBits.com>
 */

import {
  Component,
  OnInit,
  Input,
  Attribute,
  ElementRef,
  OnChanges
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Config } from './../../../environments/config';
import { ScrapService } from './../../../services/products/scrap.service';
import { CartService } from './../../../services/cart/cart.service';

/**
 *  jQuery variables
 */
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'scrap',
  styleUrls: ['./scrap.component.css'],
  templateUrl: './scrap.component.html',
  providers: [
    ScrapService,
    CartService
  ]
})
export class ScrapComponent implements OnInit, OnChanges {
  public colors: any;
  public sizes: any;
  public item: any;
  public errors: any;
  public url;
  public urlxs;
  public itemFormErrors: any;

  /**
   * Form Controls
   */
  public scrapForm: FormGroup;
  public scrapFormxs: FormGroup;
  public itemForm: FormGroup;

  constructor(
    public route: ActivatedRoute,
    public scrapService: ScrapService,
    private fb: FormBuilder,
    public cartService: CartService
  ) {
  }

  public ngOnInit() {
      $( '.menu-btn' ).click(function(){
      $('.responsive-menu').toggleClass('expand')
      });

    this.createForm();
    this.createItemForm();
  }

  public createForm() {
    this.scrapForm = this.fb.group({
      url: ['', [Validators.required]]
    });

    this.scrapForm.valueChanges.subscribe((data) => (this.ngOnChanges()));

    this.ngOnChanges();
  }

  public createItemForm() {
    this.itemForm = this.fb.group({
      price: ['', [Validators.required]],
      quantity: ['', [Validators.required]],
      size: ['', []],
      color: ['', []],
      category: ['', []],
      comment: ['', []],
      currency: ['', []]
    });
  }

  public ngOnChanges() {
    if (!this.scrapForm.value.url) {
      return;
    }

    this.scrapService.find({
      url: this.scrapForm.value.url
    }).subscribe(
      (data) => {
        this.item = data;
        this.colors = [];
        Object.keys(this.item.colors).map((key) => {
          this.colors.push(key)
        });
        this.sizes = [];
        Object.keys(this.item.sizes).map((key) => {
          this.sizes.push(key)
        });

        this.itemForm.patchValue({
          price: this.item.offer_price.new.amount,
          currency: this.item.currency,
          quantity: this.item.quantity,
          category: this.item.category,
          size: this.item.size
        });
      },
      (errors) => {
        this.errors = errors;
      }
      );
  }

  public onSubmit() {
    this.cartService.announceItemAdded(this.item);
  }

  /**
   * add item 
   */

  public changeStyle() {
    let add_btn;
    add_btn = document.getElementById('add-item');
    add_btn.innerHTML = 'added to cart <i class="glyphicon glyphicon-ok"></i>';
    add_btn.setAttribute('class', 'btn btn-success');
  }

}
