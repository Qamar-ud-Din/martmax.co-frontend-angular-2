/**
 * @author Mustafa Omran <m.omran@mQuBits.com>
 */

import {
  Component,
  OnInit,
  Input,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Config } from './../../../environments/config';
import { ListingProductsService } from './../../../services/listing/listingproducts.service';
import { CartService } from './../../../services/cart/cart.service';
import { IterateObject } from './../../pipes/iterate.object.pipe';
import { LoaderService } from './../../../services/loader/loader.service';

@Component({
  selector: 'martmax-listing-Products-by-catalog',
  styleUrls: ['./listing-products-by-catalog.component.css'],
  templateUrl: './listing-products-by-catalog.component.html',
  providers: [
    ListingProductsService,
    CartService
  ],
})
export class ListingProductsByCatalogComponent implements OnInit {
  public catalog: any;
  public errors: any;

  constructor(
    public route: ActivatedRoute,
    public listingProductsService: ListingProductsService,
    public cartService: CartService,
    private loaderService: LoaderService,
  ) {
  }

  public ngOnInit() {
    this.loaderService.display(true);
    this.listingProductsService.listingProductsByCategory().subscribe(
      (data) => {
        if (data.length === 0) {
          return;
        }
        this.catalog = data;
        this.loaderService.display(false);
      },

      (errors) => {
        this.errors = errors;
      }
    );
  }

  public addToCart(item: any) {
    this.cartService.announceItemAdded(item);
  }

  public isInCart(item: any): Boolean {
    return this.cartService.isInCart(item);
  }

  public setBrand(brand) {
    localStorage.setItem('currentBrand', JSON.stringify(brand));
  }

}
