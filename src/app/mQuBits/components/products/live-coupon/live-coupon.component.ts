/**
 * @author ahmed mahgoub <a.mahgoub@mqubits.com>
 */
import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Response} from '@angular/http';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Config } from './../../../environments/config';
import { CouponsService } from './../../../services/coupons/coupons.service';

/**
 *  jQuery variables
 */
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'live-coupon',
  styleUrls: ['./live-coupon.component.css'],
  templateUrl: './live-coupon.component.html',
  providers: [CouponsService],
})
export class LiveCouponComponent implements OnInit {
  public errors: any;
  public coupon: any;
  constructor(
    public couponsService: CouponsService,
  ) {
  }

  public ngOnInit() {
    this.get_coupon();
  }
  public get_coupon()
  {
    let response=this.couponsService.list()
    .subscribe(
        x =>{
            this.coupon=x;
            this.display(x);
            console.log(x);
            },
        e => {
        console.log(e);
        },
        () => console.log('onCompleted'));
  }
  public display(response:any)
  {
    let coupon_title=document.getElementById('coupon-title');
    if(!coupon_title) {
      return;
    }
    coupon_title.innerHTML=response.discription;
    let coupon_expire_date=document.getElementById('coupon-expire-date');
    coupon_expire_date.innerHTML="Expires: "+response.end_date;
    let code=document.getElementById('code');
    code.innerHTML=response.code;
  }
  public view_details()
  {
    window.open('/coupons-history');
  }
  
}


