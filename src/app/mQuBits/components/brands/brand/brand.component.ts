/**
 * @author Mustafa Omran <m.omran@mQuBits.com>
 */

import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Config } from './../../../environments/config';

@Component({
  selector: 'martmax-brand',
  styleUrls: ['./brand.component.css'],
  templateUrl: './brand.component.html',
  providers: [],
})
export class BrandComponent implements OnInit {
  public errors: any;
  public brand: any;

  constructor(
    public route: ActivatedRoute
  ) {
  }

  public ngOnInit() {
    let jsonBrand = localStorage.getItem('currentBrand');
    this.brand = JSON.parse(jsonBrand);
    console.log(this.brand);
  }
}
