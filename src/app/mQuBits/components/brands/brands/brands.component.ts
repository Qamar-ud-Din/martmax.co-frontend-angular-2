/**
 * @author Mustafa Omran <m.omran@mQuBits.com>
 */

import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Config } from './../../../environments/config';
import { BrandsService } from './../../../services/brands/brands.service';

@Component({
  selector: 'brands',
  styleUrls: ['./brands.component.css'],
  templateUrl: './brands.component.html',
  providers: [BrandsService],
})
export class BrandsComponent implements OnInit {
  public errors: any;
  public brands: any;
  public keys: any;

  constructor(
    public route: ActivatedRoute,
    public brandsService: BrandsService,
  ) {
  }

  public ngOnInit() {

    this.brandsService.list()
      .subscribe(
      (data) => {
        if (data.length === 0) {
          return;
        }
        this.keys = Object.keys(data.brands);
        this.brands = data.brands;
      },
      (errors) => {
        this.errors = errors;
      }
      );
  }

  public setBrand(brand) {
    localStorage.setItem('currentBrand', JSON.stringify(brand));
  }
}
