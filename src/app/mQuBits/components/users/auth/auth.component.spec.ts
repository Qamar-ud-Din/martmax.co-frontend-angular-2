import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

// Load the implementations that should be tested
import { AuthComponent } from './auth.component';

describe('Login', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      AuthComponent
    ]
  }));

  it('should log ngOnInit', inject([AuthComponent], (auth: AuthComponent) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();

    auth.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
