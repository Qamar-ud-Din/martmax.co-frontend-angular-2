import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

// Load the implementations that should be tested
import { CouponsHistoryComponent } from './coupons-history.component';

describe('CouponsHistory', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      CouponsHistoryComponent
    ]
  }));

  it('should log ngOnInit', inject([CouponsHistoryComponent], (couponshistory: CouponsHistoryComponent) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();
    couponshistory.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
