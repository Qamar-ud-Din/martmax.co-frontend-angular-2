/**
 * @author Mustafa Qamar-ud-Din <m.qamaruddin@mQuBits.com>
 */
import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  ActivatedRoute
} from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Config } from './../../../environments/config';
import { LoginService } from './../../../services/auth/login.service';
import { CartService } from './../../../services/cart/cart.service';

@Component({
  selector: 'martmax-search',
  styleUrls: ['./search.component.css'],
  templateUrl: './search.component.html',
  providers: [
    LoginService,
    CartService
  ],
})
export class SearchComponent implements OnInit {
  public errors: any;
  public isloggedIn: boolean;
  public cartNum: number;
  constructor(
    public loginService: LoginService,
    public cartService: CartService
  ) {
    //
  }
  
  public ngOnInit() {
    this.isloggedIn = this.loginService.isLoggedIn();
    this.cartNum = this.cartService.cartTotalItem();

  }

  public logedout() {
    this.loginService.logOut();
    this.isloggedIn = this.loginService.isLoggedIn();

  }

}
