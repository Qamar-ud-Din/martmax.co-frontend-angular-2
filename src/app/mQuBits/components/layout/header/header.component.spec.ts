import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

// Load the implementations that should be tested
import { HeaderComponent } from './header.component';

describe('Header', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      // provide a better mock
      {
        provide: ActivatedRoute,
        useValue: {
          data: {
            subscribe: (fn: (value: Data) => void) => fn({
              yourData: 'yolo'
            })
          }
        }
      },
      HeaderComponent
    ]
  }));

  it('should log ngOnInit', inject([HeaderComponent], (header: HeaderComponent) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();
    header.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
