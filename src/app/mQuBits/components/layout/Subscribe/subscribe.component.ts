/**
 * @author ahmed mahgoub <a.mahgoub@mqubits.com>
 */
import {
  Component,
  OnInit
}from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators,} from '@angular/forms';
import {Response} from '@angular/http';
import { Config } from './../../../environments/config';
import { SubscribeService } from './../../../services/newsletter/newsletter.service';

@Component({
  selector: 'martmax-subscribe',
  styleUrls: ['./subscribe.component.css'],
  templateUrl: './subscribe.component.html',
  providers: [SubscribeService],
})
export class SubscribeComponent implements OnInit {
  public errors: any;
  public gender:any;
  constructor(
    public subscribeService: SubscribeService
  ) {
    this.gender='Male';
  }
  public ngOnInit() {
  }
  public genderclicked(n) {
    let div, id;
    if (n == 0) {
      id = 'female_id';
      div = document.getElementById(id);
      div.setAttribute('style', 'border: 2px solid lightblue');
      div = document.getElementById('male_id');
      div.setAttribute('style', 'border: none');
      this.gender='Female';
    }
    else {
      id = 'male_id';
      div = document.getElementById(id);
      div.setAttribute('style', 'border: 2px solid lightblue');
      div = document.getElementById('female_id');
      div.setAttribute('style', 'border: none');
      this.gender='Male';
    }
  }
  public sub_clicked() {
    let E=<HTMLInputElement>document.getElementById('email');
    let response=this.subscribeService.store({email: E.value,gender: this.gender})
    .subscribe( x => {
      console.log(x);
      this.open_model(x);
    },
      e => {
      console.log(e);
      this.open_model(e.email);
      },
      () => console.log('onCompleted'));
    
  }
  public open_model(message:any)
  {
  
    // Get the modal
    let modal =document.getElementById('myModal');
    // When the user clicks on the button, open the modal  
    modal.style.display = "block";
    let modal_body=document.getElementById('modal-body');
    let img_icon=<HTMLImageElement>document.getElementById("message-icon");
    let message_title=document.getElementById("message-title");
    let string_message=String(message);
    let p2=document.getElementById("myp2");
    let p3=document.getElementById("myp3");

    if(string_message[0]+string_message[1]+string_message[2]!="Tha")
    {
      message_title.innerHTML="WARNING";
      message_title.className="message-title-f";
      img_icon.src="../../../../../assets/icon/cloud-question-mark.png";
      img_icon.className="message-icon";
      modal_body.className="modal-body1";
      p2.innerHTML="";
      p3.innerHTML="";
    }
    else
    {
      message_title.innerHTML="You're now registered";
      message_title.className="message-title-s";
      img_icon.src="../../../../../assets/icon/cloud-marked-done.png";
      img_icon.className="message-icon";
      modal_body.className="modal-body2";
      p2.innerHTML="Enjoy your shopping at Martmax.";
      p3.innerHTML="You'll receive a welcome email with your coupon code shortly.";
    }

    p3.className="message";
    let p=document.getElementById("myp");
    p.innerHTML=message; 
    // Get the elements that closes the modal
    let span = <HTMLButtonElement>document.getElementsByClassName("close-div")[0];
    let btn_ok = <HTMLButtonElement>document.getElementsByClassName("modal-ok")[0];
    
    span.onclick=function()
    {
    modal.style.display = "none";
    }
    btn_ok.onclick=function()
    {
    modal.style.display = "none";
    }
    
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
      console.log(event.target);
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
  }
}
