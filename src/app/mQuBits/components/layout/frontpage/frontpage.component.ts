/**
 * @author Mustafa Qamar-ud-Din <m.qamaruddin@mQuBits.com>
 */
import {
  Component,
  OnInit,
  Input,
  Attribute
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Config } from './../../../environments/config';
import { CATALOGS } from './../../../services/layout/catalogs.service';
import { CartService } from './../../../services/cart/cart.service';
import { BrandsService } from './../../../services/brands/brands.service';

@Component({
  selector: 'front-page',
  styleUrls: ['./frontpage.component.css'],
  templateUrl: './frontpage.component.html',
  providers: [
    CartService
  ],
})


export class FrontpageComponent implements OnInit {
  public errors: any;
  public catalogs = CATALOGS;

  constructor(
    public route: ActivatedRoute,
    public cartService: CartService
  ) {
    cartService.itemAddAnnounced$.subscribe(
      (item) => {
        // @todo : update cart counter
        console.log(item);
      }
    );
  }

  public ngOnInit() {
    //
  }

}
