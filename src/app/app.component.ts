/*
 * Angular 2 decorators and services
 */
import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { AppState } from './app.service';
import { Title }     from '@angular/platform-browser';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './app.component.css',
    './../assets/css/main.css',
    './../assets/css/font-awesome.min.css'
  ],
  templateUrl: './app.component.html',
  providers: []
})
export class AppComponent implements OnInit {
  public angularclassLogo = 'http://res.cloudinary.com/dzl9mwemk/image/upload/v1492510643/1492171333_wf6hzr.png';
  public name = 'mQuBits';
  public url = 'https://facebook.com/mQuBits';

  constructor(
    public appState: AppState,
    private titleService: Title
  ) { }

  public ngOnInit() {
    //
  }

/**
 *  set title
 */
  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }

}

/*
 * Please review the https://github.com/AngularClass/angular2-examples/ repo for
 * more angular app examples that you may copy/paste
 * (The examples may not be updated as quickly. Please open an issue on github for us to update it)
 * For help or questions please contact us at @AngularClass on twitter
 * or our chat on Slack at https://AngularClass.com/slack-join
 */
