import { Routes } from '@angular/router';
import { HomeComponent } from './home';
/**
 *  mQubits
 */
import { AuthComponent } from './mQuBits/components/users/auth/auth.component'
import { LoginComponent } from './mQuBits/components/users/login/login.component';
import { RegisterComponent } from './mQuBits/components/users/register/register.component';
import { ProfileComponent } from './mQuBits/components/users/profile/profile.component';
import { SearchPageComponent } from './mQuBits/components/products/searchpage/searchpage.component';
import { CheckoutComponent } from './mQuBits/components/ecommerce/checkout/checkout.component';
import { CartComponent } from './mQuBits/components/ecommerce/cart/cart.component';
import { ItemComponent } from './mQuBits/components/products/item/item.component';
import { BrandsComponent } from './mQuBits/components/brands/brands/brands.component';
import { BrandComponent } from './mQuBits/components/brands/brand/brand.component';
import { SizechartComponent } from './mQuBits/components/sizechart/sizechart.component';
import { ContactUsComponent } from './mQuBits/components/contact-us/contact-us.component';
import { CategorylandingpageComponent } from './mQuBits/components/category-landing-page/category-landing-page.component';
import { CommingSoonComponent } from './mQuBits/components/layout/commingsoon/comming-soon.component';
import { TrackComponent } from './mQuBits/components/products/trackorder/trackorder.component';
import { CouponsHistoryComponent } from './mQuBits/components/layout/CouponsHistory/coupons-history.component';
import { PromotionsComponent } from './mQuBits/components/promotions/promotions.component';

import { AboutComponent } from './about';
import { NoContentComponent } from './no-content';
import { DataResolver } from './app.resolver';

export const ROUTES: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },

  /**
   *  mQuBits
   */
  { path: 'users/auth', component: AuthComponent },
  { path: 'auth/login', component: LoginComponent },
  { path: 'auth/register', component: RegisterComponent },
  { path: 'products/search/:category/:keyword', component: SearchPageComponent },
  { path: 'commingsoon', component: CommingSoonComponent },
  {
    path: 'about',
    component: AboutComponent
  },
  { path: 'detail', loadChildren: './+detail#DetailModule' },
  { path: 'barrel', loadChildren: './+barrel#BarrelModule' },
  { path: 'users/profile', component: ProfileComponent },
  { path: 'checkout', component: CheckoutComponent },
  { path: 'cart', component: CartComponent },
  {
    path: 'products/:category/:title/:url',
    component: ItemComponent
  },
  {
    path: 'brands',
    component: BrandsComponent
  },
  {
    path: 'brands/:slug',
    component: BrandComponent
  },
  {
    path: 'tools/size-chart',
    component: SizechartComponent
  },
  {
    path: 'products/promotion/:category/:keyword/:pagination',
    component: CategorylandingpageComponent
  },
  {
    path: 'contact-us',
    component: ContactUsComponent
  },
  { 
    path: 'coupons-history',
   component: CouponsHistoryComponent
  },
  { 
    path: 'coupons-history/:couponid',
   component: CouponsHistoryComponent
  },
  { 
    path: 'promotions/:label',
    component: PromotionsComponent
  },
  { path: '**', component: NoContentComponent }
];
