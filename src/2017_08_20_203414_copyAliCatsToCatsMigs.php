<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CopyAliCatsToCatsMigs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $collection = \App\Models\mQuBits\CategoriesAliExpress::get();
	foreach($collection as $record){
	    $model = new \App\Models\mQuBits\CategoriesOfficial();
	    $model->category = $record->name;
	    $model->custom_duty = 0.40;
	    $model->extra_fees = 10;
            $model->sales_tax = 0.15;
            $model->notes = "lorem ipsum amet dolor";
            $model->default_weight = 0.50;
	    $model->save();
	}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
